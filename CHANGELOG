[0.1.0]
* Initial package (forked from Gogs app)

[0.1.1]
* Removed reference to Gogs

[0.1.2]
* Updated description

[0.1.3]
* Updated to version 1.1.2

[1.0.0]
* Update to version 1.1.3

[1.0.1]
* Update Git to v2.7.4-0ubuntu1.2
* Fixes critical security issue that allows remote command execution in git
* https://people.canonical.com/~ubuntu-security/cve/2017/CVE-2017-1000117.html

[1.0.2]
* Preserve SECRET_KEY across updates and restarts

[1.0.3]
* Update to version 1.1.4

[1.1.0]
* Update to version 1.2.0
* New logo!
* SECURITY: Sanitation fix from Gogs (#1461)
* Status-API
* Implement GPG api
* https://github.com/go-gitea/gitea/releases/tag/v1.2.0

[1.1.1]
* Update to version 1.2.1
* Fix PR, milestone and label functionality if issue unit is disabled (#2710) (#2714)
* Fix plain readme didn't render correctly on repo home page (#2705) (#2712)
* Fix so that user can still fork his own repository to his organizations (#2699) (#2707)
* Fix .netrc authentication (#2700) (#2708)
* Fix slice out of bounds error in mailer (#2479) (#2696)

[1.1.2]
* Update to version 1.2.2
* Add checks for commits with missing author and time (#2771) (#2785)
* Fix sending mail with a non-latin display name (#2559) (#2783)
* Sync MaxGitDiffLineCharacters with conf/app.ini (#2779) (#2780)
* Update vendor git (#2765) (#2772)
* Fix emojify image URL (#2769) (#2773)

[1.1.3]
* Update to version 1.2.3
* Only require one email when validating GPG key (#2266, #2467, #2663) (#2788)
* Fix order of comments (#2835) (#2839)

[1.2.0]
* Update to version 1.3.0

[1.3.0]
* Update to version 1.3.1
* Add documentationUrl
* Sanitize logs for mirror sync (#3057, #3082) (#3078)
* Fix missing branch in release bug (#3108) (#3117)
* Fix repo indexer and submodule bug (#3107) (#3110)
* Fix legacy URL redirects (#3100) (#3106)
* Fix redis session failed (#3086) (#3089)
* Fix issue list branch link broken (#3061) (#3070)
* Fix missing password length check when change password (#3039) (#3071)

