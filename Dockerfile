FROM cloudron/base:0.10.0

ENV VERSION 1.3.1

RUN apt-get update && \
    apt-get install -y openssh-server git && \
    rm -rf /etc/ssh_host_* && \
    rm -r /var/cache/apt /var/lib/apt/lists

ADD supervisor/ /etc/supervisor/conf.d/

RUN adduser --disabled-login --gecos 'Gitea' git
# by default, git account is created as inactive which prevents login via openssh
# https://github.com/gitlabhq/gitlabhq/issues/5304
RUN passwd -d git

RUN mkdir -p /home/git/gitea
## TODO: use redis as well
RUN curl -L https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64 -o /home/git/gitea/gitea \
    && chmod +x /home/git/gitea/gitea

# setup config paths
ADD app.ini.template /home/git/app.ini.template

# setup log paths
RUN mkdir -p /run/gitea && chown -R git:git /run/gitea
RUN sed -e 's,^logfile=.*$,logfile=/run/gitea/supervisord.log,' -i /etc/supervisor/supervisord.conf

RUN ln -s /app/data/ssh /home/git/.ssh
RUN ln -s /app/data/gitconfig /home/git/.gitconfig

ADD start.sh /home/git/start.sh

# disable pam authentication for sshd
RUN sed -e 's/UsePAM yes/UsePAM no/' -e 's/UsePrivilegeSeparation yes/UsePrivilegeSeparation no/' -i /etc/ssh/sshd_config
RUN echo "UseDNS no" >> /etc/ssh/sshd_config

CMD [ "/home/git/start.sh" ]

