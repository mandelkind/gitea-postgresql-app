This app integrates with the Cloudron SSO. Admins on Cloudron automatically
become admins on Gitea.

If you want to disable Cloudron SSO, do the following:

* Admin Panel -> Authentication -> 'cloudron' -> Uncheck 'This authentication is activated'
* Admin Panel -> Users -> Change Authentication Source to 'Local' and also give a password

You can edit `/app/data/app.ini` and add any custom configuration. See the 
[configuration cheat sheet](https://docs.gitea.io/en-us/config-cheat-sheet)
for more information.

