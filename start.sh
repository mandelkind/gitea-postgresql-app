#!/bin/bash

set -eu -o pipefail

mkdir -p /run/gitea/tmp/uploads

setup_ldap_source() {
    set -eu

    # Wait for gitea to finish db setup, before we insert ldap source in db
    while ! curl --fail http://localhost:3000/healthcheck; do
        echo "Waiting for gitea to come up"
        sleep 1
    done

    now=$(date +%s)

    # Get the existing LDAP source status. This allows the user to disable LDAP
    ldap_status=$(mysql -u"${MYSQL_USERNAME}" -p"${MYSQL_PASSWORD}" -h mysql --database="${MYSQL_DATABASE}" -N -B -e "select is_actived from login_source WHERE name='cloudron';")
    [[ -z "${ldap_status}" ]] && ldap_status="1"

    if mysql -u"${MYSQL_USERNAME}" -p"${MYSQL_PASSWORD}" -h mysql --database="${MYSQL_DATABASE}" \
        -e "REPLACE INTO login_source (id, type, name, is_actived, cfg, created_unix, updated_unix) VALUES (1,2,'cloudron',${ldap_status},'{\"Name\":\"cloudron\",\"Host\":\"${LDAP_SERVER}\",\"Port\":${LDAP_PORT},\"UseSSL\":false,\"SkipVerify\":true,\"BindDN\":\"${LDAP_BIND_DN}\",\"BindPassword\":\"${LDAP_BIND_PASSWORD}\",\"UserBase\":\"${LDAP_USERS_BASE_DN}\",\"AttributeUsername\":\"username\",\"AttributeName\":\"displayname\",\"AttributeSurname\":\"\",\"AttributeMail\":\"mail\",\"Filter\":\"(\\\\u007C(mail=%[1]s)(username=%[1]s))\",\"AdminFilter\":\"(memberof=cn=admins,${LDAP_GROUPS_BASE_DN})\"}','${now}','${now}');"; then
        echo "LDAP Authentication was setup with status ${ldap_status}"
    else
        echo "Failed to setup LDAP authentication"
        exit 1
    fi
}

# SSH_PORT can be unset to disable SSH
disable_ssh="false"
if [[ -z "${SSH_PORT:-}" ]]; then
    echo "SSH disabled"
    SSH_PORT=29418 # arbitrary port to keep sshd happy
    disable_ssh="true"
fi

if [[ ! -f "/app/data/sshd/ssh_host_ed25519_key" ]]; then
    echo "Generating ssh host keys"
    mkdir -p /app/data/sshd
    ssh-keygen -qt rsa -N '' -f /app/data/sshd/ssh_host_rsa_key
    ssh-keygen -qt dsa -N '' -f /app/data/sshd/ssh_host_dsa_key
    ssh-keygen -qt ecdsa -N '' -f /app/data/sshd/ssh_host_ecdsa_key
    ssh-keygen -qt ed25519 -N '' -f /app/data/sshd/ssh_host_ed25519_key
else
    echo "Reusing existing host keys"
fi

chmod 0600 /app/data/sshd/*_key
chmod 0644 /app/data/sshd/*.pub

sed -e "s/^Port .*/Port ${SSH_PORT}/" \
    -e "s/^#ListenAddress .*/ListenAddress 0.0.0.0/" \
    -e "s,^HostKey /etc/ssh/,HostKey /app/data/sshd/," \
    /etc/ssh/sshd_config > /run/gitea/sshd_config

cp /home/git/app.ini.template "/run/gitea/app.ini"

# create default user config file
if ! [ -f /app/data/app.ini ]; then
    cp /home/git/app.ini.template /app/data/app.ini
fi

if [ "$(crudini --get /app/data/app.ini security SECRET_KEY)" == "##SECRET_KEY" ]; then
    echo "Generating new SECRET_KEY"
    crudini --set "/app/data/app.ini" security SECRET_KEY $(pwgen -1 -s)
fi

# merge user config file
crudini --merge "/run/gitea/app.ini" < "/app/data/app.ini"

# override important values
crudini --set "/run/gitea/app.ini" database DB_TYPE mysql
crudini --set "/run/gitea/app.ini" database HOST "${MYSQL_HOST}:${MYSQL_PORT}"
crudini --set "/run/gitea/app.ini" database NAME "${MYSQL_DATABASE}"
crudini --set "/run/gitea/app.ini" database USER "${MYSQL_USERNAME}"
crudini --set "/run/gitea/app.ini" database PASSWD "${MYSQL_PASSWORD}"
crudini --set "/run/gitea/app.ini" database SSL_MODE "disable"
crudini --set "/run/gitea/app.ini" server PROTOCOL "http"
crudini --set "/run/gitea/app.ini" server DOMAIN "${APP_DOMAIN}"
crudini --set "/run/gitea/app.ini" server ROOT_URL "https://%(DOMAIN)s/"
crudini --set "/run/gitea/app.ini" server HTTP_ADDR ""
crudini --set "/run/gitea/app.ini" server HTTP_PORT "3000"
crudini --set "/run/gitea/app.ini" server DISABLE_SSH "${disable_ssh}"
crudini --set "/run/gitea/app.ini" server SSH_PORT "${SSH_PORT}"
crudini --set "/run/gitea/app.ini" server APP_DATA_PATH "/app/data/appdata"
crudini --set "/run/gitea/app.ini" repository ROOT "/app/data/repository"
crudini --set "/run/gitea/app.ini" repository.upload TEMP_PATH "/run/gitea/tmp/uploads"
crudini --set "/run/gitea/app.ini" mailer HOST "${MAIL_SMTP_SERVER}:${MAIL_SMTP_PORT}"
crudini --set "/run/gitea/app.ini" mailer USER "${MAIL_SMTP_USERNAME}"
crudini --set "/run/gitea/app.ini" mailer PASSWD "${MAIL_SMTP_PASSWORD}"
crudini --set "/run/gitea/app.ini" mailer FROM "${MAIL_FROM}"
crudini --set "/run/gitea/app.ini" mailer SKIP_VERIFY "true"
crudini --set "/run/gitea/app.ini" security INSTALL_LOCK "true"
crudini --set "/run/gitea/app.ini" log MODE "console"
crudini --set "/run/gitea/app.ini" log ROOT_PATH "/run/gitea"
crudini --set "/run/gitea/app.ini" indexer ISSUE_INDEXER_PATH "/app/data/appdata/indexers/issues.bleve"


mkdir -p /app/data/repository /app/data/ssh

chown -R git:git /app/data /run/gitea

( setup_ldap_source ) &

exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Gitea

