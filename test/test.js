#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    rimraf = require('rimraf'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Builder = require('selenium-webdriver').Builder;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);
    var server, browser = new Builder().forBrowser('chrome').build();
    var LOCATION = 'test';
    var repodir = '/tmp/testrepo';
    var app, reponame = 'testrepo';
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 5000;
    var email, token;

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        rimraf.sync(repodir);
        done();
    });

    function waitForUrl(url, done) {
        browser.wait(function () {
            return browser.getCurrentUrl().then(function (currentUrl) {
                return currentUrl === url;
            });
        }, TIMEOUT).then(function () { done(); });
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    }

    function setAvatar(done) {
        browser.get('https://' + app.fqdn + '/user/settings/avatar').then(function () {
            return browser.findElement(by.xpath('//input[@type="file" and @name="avatar"]')).sendKeys(path.resolve(__dirname, '../logo.png'));
        }).then(function () {
            return browser.findElement(by.xpath('//button[contains(text(), "Update Avatar Setting")]')).click();
        }).then(function () {
            if (app.manifest.version === '1.0.3') {
                return browser.wait(until.elementLocated(by.xpath('//p[contains(text(),"updated successfully")]')), TIMEOUT);
            } else {
                return browser.wait(until.elementLocated(by.xpath('//p[contains(text(),"Your avatar setting has been updated.")]')), TIMEOUT);
            }
        }).then(function () {
            done();
        });
    }

    function checkAvatar(done) {
return done();
        superagent.get('https://' + app.fqdn + '/avatars/a3e6f3316fc1738e29d621e6a26e93d3').end(function (error, result) {
            expect(error).to.be(null);
            expect(result.statusCode).to.be(200);
            done();
        });
    }

    function editFile(done) {
        browser.get('https://' + app.fqdn + '/' + username + '/' + reponame + '/_edit/master/newfile').then(function () {
            var cm = browser.findElement(by.xpath('//div[contains(@class,"CodeMirror")]'));
            var text = 'yo';
            return browser.executeScript('arguments[0].CodeMirror.setValue("' + text + '");', cm);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="commit_summary"]')).sendKeys('Dummy edit');
        }).then(function () {
            return browser.findElement(by.xpath('//button[contains(text(), "Commit Changes")]')).click();
        }).then(function () {
            waitForUrl('https://' + app.fqdn + '/' + username + '/' + reponame + '/src/branch/master/newfile', done);
        });
    }

    function login(done) {
        browser.get('https://' + app.fqdn + '/user/login').then(function () {
            return browser.findElement(by.id('user_name')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.id('password')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.tagName('form')).submit();
        }).then(function () {
            return browser.wait(until.elementLocated(by.linkText('Dashboard')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function addPublicKey(done) {
        var publicKey = fs.readFileSync(__dirname + '/id_rsa.pub', 'utf8');

        var sshPage;
        if (app.manifest.version === '1.0.3') {
            sshPage = 'https://' + app.fqdn + '/user/settings/ssh';
        } else {
            sshPage = 'https://' + app.fqdn + '/user/settings/keys';
        }

        browser.get(sshPage).then(function () {
            return browser.findElement(by.xpath('//div[text()="Add Key"]')).click();
        }).then(function () {
            return browser.findElement(by.id('ssh-key-title')).sendKeys('testkey');
        }).then(function () {
            return browser.findElement(by.id('ssh-key-content')).sendKeys(publicKey.trim()); // #3480
        }).then(function () {
            var button = browser.findElement(by.xpath('//button[contains(text(), "Add Key")]'));
            return browser.executeScript('arguments[0].scrollIntoView(false)', button);
        }).then(function () {
            return browser.findElement(by.xpath('//button[contains(text(), "Add Key")]')).click();
        }).then(function () {
            if (app.manifest.version === '1.0.3') {
                return browser.wait(until.elementLocated(by.xpath('//p[contains(text(), "added successfully!")]')), TIMEOUT);
            } else {
                return browser.wait(until.elementLocated(by.xpath('//p[contains(text(), "has been added.")]')), TIMEOUT);
            }
        }).then(function () {
            done();
        });
    }

    function createRepo(done) {
        var getRepoPage;
        if (app.manifest.version === '1.0.3') {
            getRepoPage = browser.get('https://' + app.fqdn).then(function () {
                return browser.findElement(by.linkText('New Repository')).click();
            }).then(function () {
                return browser.wait(until.elementLocated(by.xpath('//*[contains(text(), "New Repository")]')), TIMEOUT);
            });
        } else {
            getRepoPage = browser.get('https://' + app.fqdn + '/repo/create');
        }

        getRepoPage.then(function () {
            return browser.findElement(by.id('repo_name')).sendKeys(reponame);
        }).then(function () {
            var button = browser.findElement(by.xpath('//button[contains(text(), "Create Repository")]'));
            return browser.executeScript('arguments[0].scrollIntoView(true)', button);
        }).then(function () {
            return browser.findElement(by.id('auto-init')).click();
        }).then(function () {
            return browser.findElement(by.xpath('//button[contains(text(), "Create Repository")]')).click();
        }).then(function () {
            browser.wait(function () {
                return browser.getCurrentUrl().then(function (url) {
                    return url === 'https://' + app.fqdn + '/' + username + '/' + reponame;
                });
            }, TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function checkCloneUrl(done) {
        browser.get('https://' + app.fqdn + '/' + username + '/' + reponame).then(function () {
            return browser.findElement(by.id('repo-clone-ssh')).click();
        }).then(function () {
            return browser.findElement(by.id('repo-clone-url')).getAttribute('value');
        }).then(function (cloneUrl) {
            expect(cloneUrl).to.be('ssh://git@' + app.fqdn + ':29418/' + username + '/' + reponame + '.git');
            done();
        });
    }

    function cloneRepo(done) {
        rimraf.sync(repodir);
        var env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync('git clone ssh://git@' + app.fqdn + ':29418/' + username + '/' + reponame + '.git ' + repodir, { env: env });
        done();
    }

    function pushFile(done) {
        var env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync('touch newfile && git add newfile && git commit -a -mx && git push ssh://git@' + app.fqdn + ':29418/' + username + '/' + reponame + ' master',
                 { env: env, cwd: repodir });
        rimraf.sync('/tmp/testrepo');
        done();
    }

    function fileExists() {
        expect(fs.existsSync(repodir + '/newfile')).to.be(true);
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', function (done) {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        superagent.post('https://' + inspect.apiEndpoint + '/api/v1/developer/login').send({
            username: username,
            password: password
        }).end(function (error, result) {
            if (error) return done(error);
            if (result.statusCode !== 200) return done(new Error('Login failed with status ' + result.statusCode));

            token = result.body.token;

            superagent.get('https://' + inspect.apiEndpoint + '/api/v1/profile')
                .query({ access_token: token }).end(function (error, result) {
                if (error) return done(error);
                if (result.statusCode !== 200) return done(new Error('Get profile failed with status ' + result.statusCode));

                email = result.body.email;
                done();
            });
        });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', getAppInfo);
    it('can get the main page', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can login', login);
    it('can set avatar', setAvatar);
    it('can get avatar', checkAvatar);

    it('can add public key', addPublicKey);

    it('can create repo', createRepo);

    it('displays correct clone url', checkCloneUrl);

    it('can clone the url', cloneRepo);

    it('can add and push a file', pushFile);
    it('can edit file', editFile);

    it('can restart app', function (done) {
        execSync('cloudron restart --wait');
        done();
    });

    it('can login', login);
    it('displays correct clone url', checkCloneUrl);
    it('can clone the url', cloneRepo);
    it('file exists in repo', fileExists);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', login);
    it('can get avatar', checkAvatar);
    it('can clone the url', cloneRepo);
    it('file exists in repo', function () { expect(fs.existsSync(repodir + '/newfile')).to.be(true); });

    it('move to different location', function () {
        //browser.manage().deleteAllCookies(); // commented because of error "'Network.deleteCookie' wasn't found"
        execSync('cloudron configure --wait --location ' + LOCATION + '2', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('can get avatar', checkAvatar);
    it('displays correct clone url', checkCloneUrl);
    it('can clone the url', cloneRepo);
    it('file exists in repo', function () { expect(fs.existsSync(repodir + '/newfile')).to.be(true); });

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // check if the _first_ login via email succeeds
    it('can login via email', function (done) {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');

        login(function (error) {
            if (error) return done(error);
            execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
            done();
        });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --new --wait --appstore-id ' + app.manifest.id + ' --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can set avatar', setAvatar);
    it('can get avatar', checkAvatar);
    it('can add public key', addPublicKey);
    it('can create repo', createRepo);
    it('can clone the url', cloneRepo);
    it('can add and push a file', pushFile);

    it('can update', function () {
        execSync('cloudron install --wait --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get avatar', checkAvatar);
    it('can clone the url', cloneRepo);
    it('file exists in cloned repo', fileExists);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
